<?php

namespace App\Console\Commands;

use App\Models\Locker;
use App\Models\Log;
use App\Models\Payment;
use App\Models\ReferralTransaction;
use App\Models\Transaction;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckReferralTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Unused Referral Transaction and check rule pass';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Begin Process Check Referral Transaction\n";
        $location = storage_path()."/logs/cron/";
        Log::logFile($location,'referralTransaction',"Begin Process Check Referral Transaction CRON");

        $nowDateTime = date('Y-m-d H:i:s');

        $referralTransactionDb = ReferralTransaction::join('referral_campaigns','referral_campaigns.id','=','referral_transactions.referral_campaign_id')
            ->where('expired_date','>=',$nowDateTime)
            ->where('referral_transactions.status','PENDING')
            ->select('referral_transactions.*','referral_campaigns.rule')
            ->get();

        foreach ($referralTransactionDb as $transaction) {
            $id = $transaction->id;
            $code = $transaction->code;
            $fromLockerId = $transaction->from_locker_id;
            $toLockerId = $transaction->to_locker_id;
            $type = $transaction->type;
            $fromAmount = $transaction->from_amount;
            $toAmount = $transaction->to_amount;
            $rule = $transaction->rule;

            echo "check $id: $code, $fromLockerId:$fromAmount -> $toLockerId:$toAmount\n";
            Log::logFile($location,'referralTransaction',"check $id: $code, $fromLockerId:$fromAmount -> $toLockerId:$toAmount");

            // check if success rule
            $checkRulePass = ReferralTransaction::checkRulePass($toLockerId,$type,$rule);
            if (!$checkRulePass->isSuccess){
                // check if failed
                if ($checkRulePass->isFailed){
                    // update current transaction to failed
                    $referralDb = ReferralTransaction::find($id);
                    $referralDb->status = 'FAILED';
                    $referralDb->save();

                    $errorMsg = $checkRulePass->errorMsg." Set FAILED";
                    echo "$errorMsg\n";
                    Log::logFile($location,'referralTransaction',"isFailed: TRUE $errorMsg");
                    continue;
                } else {
                    $errorMsg = $checkRulePass->errorMsg;
                    echo "$errorMsg\n";
                    Log::logFile($location,'referralTransaction',"isFailed: FALSE $errorMsg");
                    continue;
                }
            }

            $toLockerDb = Locker::find($toLockerId);

            DB::beginTransaction();

            $processReferral = ReferralTransaction::processReferral($fromLockerId,$toLockerId,$fromAmount,$toAmount,$code,$id);
            if (!$processReferral->isSuccess){
                echo $processReferral->erroMsg;
                Log::logFile($location,'referralTransaction',"Error Process: ".$processReferral->errorMsg);
                continue;
            }

            DB::commit();
            echo "== Commit ==";
            Log::logFile($location,'referralTransaction',"== Commit ==");
        }
    }
}
