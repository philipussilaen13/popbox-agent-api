<?php

namespace App\Console\Commands;

use App\Http\Helpers\ApiPopExpress;
use App\Models\DeliveryCity;
use App\Models\DeliveryDistrict;
use App\Models\DeliveryProvince;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExpressUpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'express:update-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Express Update Province,City,District Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "begin\n";
        // get destination
        $dataDb = DB::connection('popbox_express')->table('international_codes')->get();

        $newArray = [];
        foreach ($dataDb as $item) {
            $tmp = new \stdClass();
            $tmp->id = $item->detail_code;
            $tmp->name = $item->district;
            $newArray[$item->province][$item->county][] = $tmp;
        }

        foreach ($newArray as $provinceName => $cityName){
            // check on province table
            $provinceDb = DeliveryProvince::where('province_name',$provinceName)->first();
            if (!$provinceDb){
                $provinceDb = new DeliveryProvince();
                $provinceDb->province_name = $provinceName;
                $provinceDb->save();
            }
            echo "province Name \n";
            foreach ($cityName as $name => $districtList) {
                // check on city table
                $cityDb = DeliveryCity::where('delivery_province_id',$provinceDb->id)
                    ->where('city_name',$name)
                    ->first();
                if (!$cityDb){
                    $cityDb = new DeliveryCity();
                    $cityDb->delivery_province_id = $provinceDb->id;
                    $cityDb->city_name = $name;
                    $cityDb->save();
                }
                echo "$name\n";
                foreach ($districtList as $item) {
                    // check on district table
                    $districtDb = DeliveryDistrict::where('code',$item->id)->first();
                    if (!$districtDb){
                        $districtDb = new DeliveryDistrict();
                        $districtDb->delivery_city_id = $cityDb->id;
                        $districtDb->code = $item->id;
                        $districtDb->district_name = $item->name;
                        $districtDb->save();
                    }
                    echo "$item->id - $item->name\n";
                }
            }
        }
    }
}
