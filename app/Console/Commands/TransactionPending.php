<?php

namespace App\Console\Commands;

use App\Models\BalanceRecord;
use App\Models\Locker;
use App\Models\Log;
use App\Models\PendingTransaction;
use App\Models\Transaction;
use App\Models\TransactionHistory;
use App\Models\TransactionItem;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TransactionPending extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Pending Transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log("Begin Pending Transaction");

        $pendingTransactionDb = PendingTransaction::with(['user','user.locker'])
            ->where('status','PENDING')
            ->get();

        foreach ($pendingTransactionDb as $item) {
            DB::beginTransaction();
            $id = $item->id;
            $userId = $item->user_id;
            $type = $item->type;
            $reference = $item->reference;
            $transactionIdReference = $item->transaction_id_reference;
            $description = $item->description;
            $totalPrice = $item->total_price;
            $status = $item->status;
            $expiredDate  = $item->expired_date;
            $lockerId = $item->user->locker_id;

            $this->log("Process Pending ID $id,$type,$reference,$lockerId");

            $pendingDb = PendingTransaction::find($id);
            // check expired date
            $nowDateTime = date('Y-m-d H:i:s');
            if (strtotime($expiredDate) < strtotime($nowDateTime)) {
                $pendingDb->status = 'EXPIRED';
                $pendingDb->save();
                DB::commit();
                $this->log('Set expired');
                continue;
            }

            // check agent status
            $lockerDb = $item->user->locker;
            if (!$lockerDb){
                $this->log("Locker Db Not Found");
                DB::rollback();
                continue;
            }
            $agentStatus = $lockerDb->status_verification;
            if ($agentStatus != 'verified'){
                $this->log("Agent Status Not Verified. $agentStatus");
                DB::rollback();
                continue;
            }

            // create transaction based on type
            if ($type == 'commission'){
                $commissionTransaction = new Transaction();
                $commissionTransaction->users_id = $userId;
                $commissionTransaction->transaction_id_reference = $transactionIdReference;
                $commissionTransaction->type = 'commission';
                $commissionTransaction->reference = $reference;
                $commissionTransaction->description = $description;
                $commissionTransaction->total_price = $totalPrice;
                $commissionTransaction->status = 'PAID';
                $commissionTransaction->save();

                \Log::debug('Checking region from user '.$userId);
                // top up deposit
                $mresult = DB::connection('popbox_agent')
                    ->table('users')
                    ->leftJoin('popbox_virtual.lockers', 'users.locker_id', '=', 'popbox_virtual.lockers.locker_id')
                    ->leftJoin('popbox_virtual.cities', 'popbox_virtual.lockers.cities_id', '=', 'popbox_virtual.cities.id')
                    ->leftJoin('popbox_virtual.regions', 'popbox_virtual.cities.regions_id', '=', 'popbox_virtual.regions.id')
                    ->where('popbox_virtual.lockers.type', '=', 'warung')
                    ->where('popbox_virtual.regions.region_code', 'like', 'KIMONU%')
                    ->where('users.id', '=', $userId)
                    ->select('users.id', 'users.email', 'users.locker_id', 'popbox_virtual.lockers.locker_name')
                    ->count();
                
                if($mresult == 0){
                    \Log::debug('None KIMONU region users');
                    $depositDb = BalanceRecord::creditDeposit($lockerId,$totalPrice,$commissionTransaction->id);
                    if (!$depositDb->isSuccess){
                        DB::rollback();
                        $this->log($depositDb->errorMsg);
                        continue;
                    }
                }
            }
            elseif ($type == 'referral') {
                $this->log("Its referral from user_id ".$userId);
                // save to DB
                $transactionDb = new Transaction();
                $transactionDb->users_id =$userId;
                // create transaction reference
                $transactionDb->reference = $reference;
                $transactionDb->type = 'referral';
                $transactionDb->cart_id = null;
                $transactionDb->description = $description;

                // create total price
                $transactionDb->total_price = $totalPrice;
                $transactionDb->status = 'PAID';
                $transactionDb->save();

                // get transaction Id for transaction item db
                // insert initial amount topup
                $tmp = number_format($totalPrice);
                $transactionId = $transactionDb->id;
                $items = [];
                $item = new \stdClass();
                $item->type = 'referral';
                $item->price = $totalPrice;
                $item->name = "Top Up Referral $tmp";
                $item->picture = null;
                $item->params = null;
                $items[] = $item;

                $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userId);
                if (!$itemDb->isSuccess){
                    DB::rollback();
                    $this->log($itemDb->errorMsg);
                    continue;
                }

                // insert to history
                $userDb = User::find($userId);
                $user = $userDb->name;
                $remarks = "Create TopUp Transaction $totalPrice $description";
                $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'PAID',$remarks);
                if (!$historyDb->isSuccess){
                    DB::rollback();
                    $this->log("Failed Create Transaction History");
                    continue;
                }

                // find another referral transaction
                $referralPending = PendingTransaction::where('type','referral')
                    ->where('id',$transactionIdReference)
                    ->first();
                if ($referralPending){
                    $referralPending = PendingTransaction::find($referralPending->id);
                    // save to DB
                    $transactionDb = new Transaction();
                    $transactionDb->users_id =$referralPending->user_id;
                    // create transaction reference
                    $transactionDb->reference = $referralPending->reference;
                    $transactionDb->type = 'referral';
                    $transactionDb->cart_id = null;
                    $transactionDb->description = $referralPending->description;

                    // create total price
                    $transactionDb->total_price = $referralPending->total_price;
                    $transactionDb->status = 'PAID';
                    $transactionDb->save();

                    // get transaction Id for transaction item db
                    // insert initial amount topup
                    $tmp = number_format($referralPending->total_price);
                    $transactionId = $transactionDb->id;
                    $items = [];
                    $item = new \stdClass();
                    $item->type = 'referral';
                    $item->price = $referralPending->total_price;
                    $item->name = "Top Up Referral $tmp";
                    $item->picture = null;
                    $item->params = null;
                    $items[] = $item;

                    $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$referralPending->user_id);
                    if (!$itemDb->isSuccess){
                        DB::rollback();
                        $this->log($itemDb->errorMsg);
                        continue;
                    }

                    // insert to history
                    $userDb = User::find($referralPending->user_id);
                    $user = $userDb->name;
                    $remarks = "Create TopUp Transaction $totalPrice $description";
                    $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'PAID',$remarks);
                    if (!$historyDb->isSuccess){
                        DB::rollback();
                        $this->log("Failed Create Transaction History");
                        continue;
                    }
                    $referralPending->status = 'USED';
                    $referralPending->save();
                }
            }
            else {
                DB::rollback();
                $this->log("Invalid Type");
                continue;
            }

            $pendingDb->status = 'USED';
            $pendingDb->save();

            $this->log("End Used");
            DB::commit();
        }

        $this->log("End Pending Transaction");
    }

    private function log($message){
        echo "$message\n";
        $location = storage_path()."/logs/cron/";
        Log::logFile($location,'transactionPending',"$message");
    }
}
