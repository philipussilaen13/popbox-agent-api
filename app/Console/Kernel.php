<?php

namespace App\Console;

use App\Console\Commands\BackupDatabase;
use App\Console\Commands\ChangePermission;
use App\Console\Commands\CheckReferralTransaction;
use App\Console\Commands\ExpiredPayment;
use App\Console\Commands\ExpressUpdateData;
use App\Console\Commands\LockerNameReferral;
use App\Console\Commands\LockerVirtualAccountOpen;
use App\Console\Commands\NotificationSendFCM;
use App\Console\Commands\ProcessDokuVA;
use App\Console\Commands\SepulsaSerialNumber;
use App\Console\Commands\ApacheBackup;
use App\Console\Commands\TransactionCommissionReference;
use App\Console\Commands\TransactionPending;
use App\Console\Commands\TransactionReport;
use App\Models\PendingTransaction;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ExpiredPayment::class,
        ProcessDokuVA::class,
        BackupDatabase::class,
        LockerNameReferral::class,
        SepulsaSerialNumber::class,
        LockerVirtualAccountOpen::class,
        CheckReferralTransaction::class,
        ExpressUpdateData::class,
        ApacheBackup::class,
        ChangePermission::class,
        TransactionCommissionReference::class,
        NotificationSendFCM::class,
        TransactionPending::class,
        TransactionReport::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $nowDate = date('Y.m.d');
        // $schedule->command('inspire')-
        //          ->hourly();
        //$schedule->command('queue:work --tries=3 --sleep=3')->everyMinute()->withoutOverlapping();
        $schedule->command('permission:change')->everyMinute()->withoutOverlapping();
        $schedule->command('payment:expired')->everyMinute()->withoutOverlapping();
        //$schedule->command('payment:dokuva')->everyMinute()->withoutOverlapping(); #Deprecated, not use. Change with new payment using callback
        $schedule->command('backupdb')->dailyAt('01:00');
        $schedule->command('locker:name')->everyTenMinutes()->withoutOverlapping();
        $schedule->command('transaction:sepulsa-sn')->everyMinute()->withoutOverlapping();
        $schedule->command('locker:vaopen')->hourly()->withoutOverlapping();
        $schedule->command('referral:transaction')->everyMinute()->withoutOverlapping();
        $schedule->command('express:update-data')->weekly()->sundays()->at('02:00');
        // $schedule->command('transaction:reference')->everyMinute()->withoutOverlapping(); #for fix transaction reference
        $schedule->command('notifications:send-fcm')->everyMinute()->withoutOverlapping();
        $schedule->command('transaction:pending')->everyMinute()->withoutOverlapping();
        $schedule->command('transaction:report')->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        // $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
