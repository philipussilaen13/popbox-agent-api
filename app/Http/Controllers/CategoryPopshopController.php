<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Models\DimoCategoryName;
use Excel;

class CategoryPopshopController extends Controller
{
    public function changeListCategory(Request $request)
    {
        $request->hasFile('file');
        $path = $request->file('file')->getRealPath();
        $data =  Excel::load($path)->get();
        $insert = [];
        $no_list = [];
        if (!empty($data) && $data->count() > 0 ) {
            foreach ($data as $key => $value) {
                $no_list[] = (int)$value->no_list;
                $insert[] = [
                    'id_category' => (int)$value->id_category,
                    'category_name' => $value->category_name,
                    'no_list' => (int)$value->no_list,
                ];
            }
        }

        $count = 0;
        $duplicate = [];
        $data = array_count_values($no_list);
        foreach ($data as $key => $value) {
           if ($value > 1) {
                $list[] = $key;
                $duplicate = [
                    'duplicate_no_list' => $list
                ];
           }
        }

        $change = [
            'message' => 'No data Change',
            'data' => $duplicate
        ];
        
        if (empty($duplicate)) {
            $change = [];
            foreach ($insert as $key => $value) {
                $category = DimoCategoryName::find($value['id_category']);
                if ($category->no_list != $value['no_list']) {
                    $change[] = [
                        'id_category' => (int)$value['id_category'],
                        'category_name' => $value['category_name'],
                        'no_list' => (int)$value['no_list'],
                    ];
                    $category->no_list = $value['no_list'];
                    $category->save();
                }
            }
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$change]];
        return response()->json($resp);
    }
}
