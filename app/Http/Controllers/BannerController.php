<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;
use App\Models\WarungBanners;

class BannerController extends Controller
{
    public function list(Request $request){
        
        $required = ['locker_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        $locker_id = $request->get('locker_id');
        
        $user = Helper::getUserInfo($locker_id);
        
        $banners = WarungBanners::where('user_type', $user->type)->where('status', 'active')->select(DB::raw("id, title, CONCAT('".env('AGENT_URL')."banner/'".", image_name) as 'image_url'"))->orderBy('updated_at', 'desc')->get();
        
        $resp = ['response' => ['code' => 200,'message' =>null], 'data' => $banners];
        
        return response()->json($resp);
    }
}
