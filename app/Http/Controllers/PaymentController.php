<?php

namespace App\Http\Controllers;

use App\Jobs\CreateTransactionNotification;
use App\Jobs\ProcessTransaction;
use App\Jobs\SendFCM;
use App\Models\Company;
use App\Models\Locker;
use App\Models\LockerVAOpen;
use App\Models\Log;
use App\Models\NotificationFCM;
use App\Models\Payment;
use App\Models\PaymentDokuVA;
use App\Models\PaymentMethod;
use App\Models\PaymentVirtualAccount;
use App\Models\SmsNotification;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Transaction as TransactionMail;

class PaymentController extends Controller
{
    /**
     * get payment method
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMethod(Request $request){
        $required = ['transaction_ref'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // check transaction
        $reference = $request->input('transaction_ref');
        $transactionDb = Transaction::where('reference',$reference)->first();
        if (!$transactionDb){
            $message = 'Invalid Transaction Reference';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get from DB
        $methodDb = PaymentMethod::getPaymentMethod($input);
        $methodList = [];
        foreach ($methodDb as $item){
            $tmp = new \stdClass();
            $tmp->module = $item->module;
            $tmp->code = $item->code;
            $tmp->name = $item->name;
            $methodList[] = $tmp;
        }
        if (empty($methodList)){
            $message = "Payment Method Not Available";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $methodList];
        return response()->json($resp);
    }

    /**
     * create payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPayment(Request $request){
        
        $required = ['transaction_ref','method'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get variable
        $username = $request->input('username');
        $transactionRef = $request->input('transaction_ref');
        $method = $request->input('method');

        // insert into DB
        DB::beginTransaction();
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // create result
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * pay payment for cash method
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paidPayment(Request $request){
        $required = ['transaction_ref','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $transactionRef = $request->input('transaction_ref');
        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        // update to DB
        DB::beginTransaction();
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        DB::commit();

        // create result
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // send sms for popshop
        $isPopShop = false;
        foreach ($data->items as $item) {
            if (!$isPopShop){
                if ($item->type == 'popshop') $isPopShop = true;
            }
        }

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // if transaction contain popshop
        if ($isPopShop){
            $userDb = $transactionDb->user;
            if ($userDb){
                $phone = $userDb->phone;
                $tmpAmount = number_format($transactionDb->total_price);
                $message = "Order BELANJA sukses $transactionRef sejumlah Rp$tmpAmount. Estimasi waktu pengiriman adl 3-5 hari kerja. Terima kasih";

                $smsNotif = new SmsNotification();
                $smsNotif->createSMS('paid_transaction','nexmo',$phone,$message);
            }
        }

//         $lockerDB = collect(\DB::connection('mysql')
//             ->select("
//                 select b.type
//                 from users a
//                 left join popbox_virtual.lockers b on a.locker_id = b.locker_id
//                 where a.username = ?                        
//             ",[$username]))->first();

//         if($lockerDB->type == 'warung') {
            $transactionMailDB = Transaction::getEmailTransaction($transactionRef);
            if( sizeof($transactionMailDB) > 0) {
                Mail::to('popstore@popbox.asia')->send(new TransactionMail($transactionMailDB->data));
                if( count(Mail::failures()) > 0 ) {
                    \Log::info('Mail Transaction Warung ('.$transactionRef.') ' . date("Y-m-d H:i:s") . ' has been failure');    
                }
            }    
//         }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * detail payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function detailPayment(Request $request){
        $required = ['transaction_ref'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $request->input('transaction_ref');

        // get payment detail
        $paymentDb = Payment::detailPayment($transactionRef);
        if (!$paymentDb->isSuccess){
            $resp=['response' => ['code' => 400,'message' =>$paymentDb->errorMsg], 'data' => []];
            return response()->json($resp);
        }
        $data = $paymentDb->data;
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * create paid transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPaidTransaction(Request $request){
        $required = ['item_name','item_type','item_price','item_param','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get variable
        $username = $request->input('username');
        $itemName = $request->input('item_name');
        $itemType = $request->input('item_type');
        $itemPrice = $request->input('item_price');
        $itemPicture = $request->input('item_picture',null);
        $itemParam = $request->input('item_param');
        $agentPassword = $request->input('agent_password');

        DB::beginTransaction();
        // create transaction
        $transactionDb = Transaction::createSingleTransaction($username,$itemName,$itemType,$itemPrice,$itemParam,$itemPicture);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // create parameter for FCM Notification
        $userDb = User::where('username',$username)->first();
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                NotificationFCM::createTransactionNotification($transactionDb->id,$fcmToken,$data,'Transaksi Sedang di Proses','process','Transactions');
            }
            dispatch(new SendFCM());
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Callback for fixed
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentCallback(Request $request){
        $response = new \stdClass();
        $response->is_success =false;
        $response->error_msg = null;
        $location = storage_path()."/logs/";
        Log::logFile($location,'callback',"Begin Callback Payment");

        $required = ['token','transaction_id','status'];
        // get all param
        $input = $request->input();
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response->error_msg = $message;
            Log::logFile($location,'callback',"Failed $message");
            return response()->json($response);
        }

        $token = $request->input('token');
        $reference = $request->input('transaction_id');
        $paymentId = $request->input('payment_id');
        $statusPayment = $request->input('status');
        $paymentChannelCode = $request->input('payment_channel_code');

        // check token
        $companyDb = Company::where('token',$token)->first();
        if (!$companyDb){
            $response->error_msg = 'Invalid Token';
            Log::logFile($location,'callback',"Failed Invalid Token");
            return response()->json($response);
        }

        // find transaction
        $transactionDb = Transaction::where('reference',$reference)->first();
        if (!$transactionDb){
            $response->error_msg = 'Invalid Transaction Id';
            Log::logFile($location,'callback',"Failed Invalid Transaction");
            return response()->json($response);
        }
        $transactionStatus = $transactionDb->status;
        Log::logFile($location,'callback',"$reference status $transactionStatus | payment status $statusPayment, $paymentId");

        DB::beginTransaction();
        // change transaction and payment status to paid
        $transactionDb = Transaction::find($transactionDb->id);
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        $paymentDb = Payment::where('transactions_id',$transactionDb->id)->first();

        $paymentData = Payment::find($paymentDb->id);
        $paymentData->status = 'PAID';
        $paymentData->save();

        // check on payment virtual account
        $checkPaymentVirtualAccount = PaymentVirtualAccount::where('payments_id',$paymentDb->id)
            ->where('status','PAID')
            ->first();
        if ($checkPaymentVirtualAccount){
            DB::rollback();
            $response->is_success = true;
            $response->error_msg = "$reference Already Exist and PAID";
            Log::logFile($location,'callback',"$reference Already Exist and PAID");
            return response()->json($response);
        }

        switch ($paymentChannelCode) {
            case 'BNI-VA' :
                Log::logFile($location,'callback',"Update Payment Virtual Account");
                $paymentVirtualDb = PaymentVirtualAccount::where('payments_id',$paymentDb->id)->first();
                $paymentVirtualDb = PaymentVirtualAccount::find($paymentVirtualDb->id);
                $paymentVirtualDb->status = 'PAID';
                $paymentVirtualDb->save();
                break;
            case 'DOKU-MANDIRI' :
            case 'DOKU-PERMATA' :
            case 'DOKU-ALFAMART' :
                Log::logFile($location,'callback',"Update Payment Virtual Account");
                $paymentVirtualDb = PaymentVirtualAccount::where('payments_id',$paymentDb->id)->first();
                if ($paymentVirtualDb){
                    $paymentVirtualDb = PaymentVirtualAccount::find($paymentVirtualDb->id);
                    $paymentVirtualDb->status = 'PAID';
                    $paymentVirtualDb->save();
                } else {
                    $paymentDokuDb = PaymentDokuVA::where('payments_id',$paymentDb->id)->first();
                    $paymentDokuDb = PaymentDokuVA::find($paymentDokuDb->id);
                    $paymentDokuDb->status = 'PAID';
                    $paymentDokuDb->save();
                }
                break;
        }
        $result = $this->success($transactionDb);
        if (!$result->isSuccess){
            $response->error_msg = $result->errorMsg;
            Log::logFile($location,'callback',$result->errorMsg);
            return response()->json($response);
        }

        Log::logFile($location,'callback',"Update Transaction and Payment");

        DB::commit();
        $response->is_success = true;
        return response()->json($response);
    }

    /**
     * Callback for open Payment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentOpenCallback(Request $request){
        $response = new \stdClass();
        $response->is_success =false;
        $response->error_msg = null;
        $location = storage_path()."/logs/";
        Log::logFile($location,'callback',"Begin Callback Payment");

        $required = ['token','payment_id','sub_payment_id','status'];
        // get all param
        $input = $request->input();
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response->error_msg = $message;
            Log::logFile($location,'callback',"Failed $message");
            return response()->json($response);
        }

        $token = $request->input('token');
        $paymentId = $request->input('payment_id');
        $subPaymentId = $request->input('sub_payment_id');
        $statusPayment = $request->input('status');
        $paymentChannelCode = $request->input('payment_channel_code');
        $paidAmount = $request->input('paid_amount');
        $lastPaidAmount = $request->input('last_paid_amount');
        $adminFee = $request->input('customer_admin_fee',0);

        // check token
        $companyDb = Company::where('token',$token)->first();
        if (!$companyDb){
            $response->error_msg = 'Invalid Token';
            Log::logFile($location,'callback',"Failed Invalid Token");
            return response()->json($response);
        }

        // find transaction va locker
        $lockerVAOpen = LockerVAOpen::where('payment_id',$paymentId)->first();
        if (!$lockerVAOpen){
            $response->error_msg = 'Invalid Payment ID';
            Log::logFile($location,'callback',"Failed Invalid Payment ID");
            return response()->json($response);
        }

        $lockerId = $lockerVAOpen->lockers_id;
        $vaType = $lockerVAOpen->va_type;
        $vaNumber = $lockerVAOpen->va_number;

        Log::logFile($location,'callback',"Open VA $vaType $vaNumber| payment status $statusPayment, $paymentId, $lastPaidAmount");

        DB::beginTransaction();

        $userDb = User::where('locker_id',$lockerId)->first();
        if (!$userDb){
            DB::rollback();
            $response->error_msg = 'User Not Found';
            Log::logFile($location,'callback','User Not Found');
            return response()->json($response);
        }

        $username = $userDb->username;

        // check exist on payment virtual account
        $checkVirtualAccountPayment = PaymentVirtualAccount::where('payment_id',$subPaymentId)->first();
        if ($checkVirtualAccountPayment){
            DB::rollback();
            // if exist then true
            $response->is_success = true;
            $response->error_msg = "$subPaymentId Already Exist in Payment Virtual Account";
            Log::logFile($location,'callback',"$subPaymentId Already Exist in Payment Virtual Account");
            return response()->json($response);
        }

        // create transaction
        $transactionDb = Transaction::createTopUpOpenTransaction($username,$lastPaidAmount,$vaType,$subPaymentId,$adminFee);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // create payment
        $paymentDb = Payment::addPayment($transactionRef,$username,$vaType);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // change transaction and payment status to paid
        $transactionDb = Transaction::find($transactionDb->transactionId);
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        $paymentDb = Payment::where('transactions_id',$transactionDb->id)->first();

        $paymentData = Payment::find($paymentDb->id);
        $paymentData->status = 'PAID';
        $paymentData->save();

        // create payment virtual Account
        $createVA = PaymentVirtualAccount::createPayment($paymentData->id,$subPaymentId,'open',$vaNumber,'PAID');
        if (!$createVA->isSuccess){
            DB::rollback();
            $message = 'Failed Create Payment Virtual Account';
            Log::logFile($location,'callback',$message);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // dispatch job top up or process transaction
        $result = $this->success($transactionDb);

        if (!$result->isSuccess){
            $response->error_msg = $result->errorMsg;
            Log::logFile($location,'callback',$result->errorMsg);
            return response()->json($response);
        }

        Log::logFile($location,'callback',"Update Transaction and Payment");

        DB::commit();
        $response->is_success = true;
        return response()->json($response);
    }

    /**
     * @param $transactionDb
     * @return \stdClass
     */
    private function success($transactionDb){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $location = storage_path()."/logs/";
        if ($transactionDb->type=='topup'){
            Log::logFile($location,'callback',"Process TopUp");
            
            \Log::debug('Checking region from user '.$transactionDb->users_id);
            // top up deposit
            $mresult = User::leftJoin('popbox_virtual.lockers', 'users.locker_id', '=', 'popbox_virtual.lockers.locker_id')
                            ->leftJoin('popbox_virtual.cities', 'popbox_virtual.lockers.cities_id', '=', 'popbox_virtual.cities.id')
                            ->leftJoin('popbox_virtual.regions', 'popbox_virtual.cities.regions_id', '=', 'popbox_virtual.regions.id')
                            ->join('transactions', 'transactions.users_id', '=', 'users.id')
                            ->join('payments', 'payments.transactions_id', '=', 'transactions.id')
                            ->join('payment_methods', 'payment_methods.id', '=', 'payments.payment_methods_id')
                            ->where('popbox_virtual.lockers.type', '=', 'warung')
                            ->where('popbox_virtual.regions.region_code', 'like', 'KIMONU%')
                            ->where('transactions.id', '=', $transactionDb->id)
                            ->where('users.id', '=', $transactionDb->users_id)
                            ->where('payment_methods.code', '=', 'otto_qr')
                            ->select('users.id', 'users.email', 'users.locker_id', 'popbox_virtual.lockers.locker_name')
                            ->count();
            
            if($mresult == 0){
                \Log::debug('Processing top up...');
                $result = Locker::processTopUp($transactionDb);
                if (!$result->isSuccess){
                    $response->errorMsg = $result->errorMsg;
                    return $response;
                }
                \Log::debug('Process DONE');
            }

            $response->isSuccess = true;
        } else {
            Log::logFile($location,'callback',"Dispatch Transaction");
            dispatch(new ProcessTransaction($transactionDb));
            dispatch(new CreateTransactionNotification($transactionDb));
            $response->isSuccess = true;
        }
        return $response;
    }
}
