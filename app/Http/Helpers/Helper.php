<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 16/02/2017
 * Time: 16:38
 */

namespace App\Http\Helpers;

use App\Models\Virtual\Locker;


class Helper
{
    /**
     * generate random string
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10) {
        $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $latitudeFrom
     * @param $longitudeFrom
     * @param $latitudeTo
     * @param $longitudeTo
     * @param int $earthRadius in meters
     * @return int
     */
    public static function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
    
    public static function like_match($pattern, $subject) {
        $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
        return (bool) preg_match("/^{$pattern}$/i", $subject);
    }
    
    public static function getMinimumPurchaseByUserId($userId){
        return \DB::select('select distinct ROUND(popbox_virtual.regions.min_purchase, 0) as "min_purchase"
                    from popbox_virtual.regions
                    join popbox_virtual.cities on popbox_virtual.cities.regions_id = popbox_virtual.regions.id
                    join popbox_virtual.lockers on popbox_virtual.lockers.cities_id = popbox_virtual.cities.id
                    join users on users.locker_id = popbox_virtual.lockers.locker_id
                    where users.id = '.$userId);
    }
    
    public static function getRegionInfo($locker_id){
        return Locker::join('popbox_virtual.cities', 'popbox_virtual.lockers.cities_id', '=', 'popbox_virtual.cities.id')
                ->join('popbox_virtual.regions', 'popbox_virtual.regions.id', '=', 'popbox_virtual.cities.regions_id')
                ->where('popbox_virtual.lockers.locker_id', '=', $locker_id)
                ->select('popbox_virtual.regions.id', 'popbox_virtual.regions.region_code')
                ->get();
    }
    
    public static function getUserInfo($locker_id){
        return Locker::join('popbox_virtual.cities', 'popbox_virtual.lockers.cities_id', '=', 'popbox_virtual.cities.id')
        ->join('popbox_virtual.regions', 'popbox_virtual.regions.id', '=', 'popbox_virtual.cities.regions_id')
        ->join('popbox_agent.lockers', 'popbox_agent.lockers.id', '=', 'popbox_virtual.lockers.locker_id')
        ->join('popbox_agent.users', 'popbox_agent.users.locker_id', '=', 'popbox_agent.lockers.id')
        ->where('popbox_virtual.lockers.locker_id', '=', $locker_id)
        ->select('popbox_virtual.regions.id', 'popbox_virtual.regions.region_code', 'popbox_virtual.lockers.type')
        ->first();
        
    }
    
    public static function unsetProp($data, $props){
        $rows = [];
        foreach ($data as $row){
            foreach ($props as $prop){
                unset($row[$prop]);
            }
            $rows[] = $row;
        }
        
        return $rows;
    }
}