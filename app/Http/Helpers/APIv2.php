<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 13/02/2017
 * Time: 14:09
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class APIv2
{
    private $id = null;
    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;
        if (\Session::has('auth.phone')) {
            $phone = \Session::get('auth.phone');
            $unique = $this->id.' > '.$phone;
        }

        $host = env('API_URL');
        $token = env('API_TOKEN');

        if(substr($host, strlen($host) - 1) != '/'){
            $url = $host.'/'.$request;
        } else {
            $url = $host.$request;
        }
        
        $param['token'] = $token;
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $user = get_current_user();
        $f = fopen(storage_path().'/logs/api/'.$date.'.'.$user.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type:application/json'));
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path().'/logs/api/'.$date.'.'.$user.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $json,
                'api_response'  => $output,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $output;
    }

    /**
     * get pulsa list based on phone
     * @param $phone
     * @return mixed
     */
    public function getPulsaProduct($phone = null, $operator = null){
        $url = 'service/sepulsa/getPulsaProduct';
        $param = [];
        $param['phone'] = $phone;
        $param['operator'] = $operator;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get PLN Pre Paid Product
     * @return mixed
     */
    public function getPLNPrePaidProduct(){
        $url = 'service/sepulsa/getPLNProduct';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get PLN PrePaid Inquiry
     * @param $meterNumber
     * @param $productId
     * @return mixed
     */
    public function getPLNPrePaidInquiry($meterNumber,$productId){
        $url = 'service/sepulsa/getElectricityPrepaid';
        $param = [];
        $param['meter_number'] = $meterNumber;
        $param['product_id'] = $productId;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get PLN Post Paid Inquiry
     * @param $meterNumber
     * @return mixed
     */
    public function getPLNPostPaidInquiry($meterNumber){
        $url = 'service/sepulsa/getElectricityPostpaid';
        $param = [];
        $param['meter_number'] = $meterNumber;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get BPJS Inquiry
     * @param $bpjsNumber
     * @return mixed
     */
    public function getBPJSInquiry($bpjsNumber){
        $url = 'service/sepulsa/getBPJSInquiry';
        $param = [];
        $param['bpjs_number'] = $bpjsNumber;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get PDAM Operator
     * @return mixed
     */
    public function getPDAMOperator(){
        $url = 'service/sepulsa/getPDAMProduct';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get PDAM Inquiry
     * @param $pdamNumber
     * @param $operatorCode
     * @return mixed
     */
    public function getPDAMInquiry($pdamNumber,$operatorCode){
        $url = 'service/sepulsa/getPDAMInquiry';
        $param = [];
        $param['pdam_number'] = $pdamNumber;
        $param['operator_code'] = $operatorCode;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get Telkom Number
     * @param $telkomNumber
     * @return mixed
     */
    public function getTelkomInquiry($telkomNumber){
        $url = 'service/sepulsa/getTelkomInquiry';
        $param = [];
        $param['telkom_number'] = $telkomNumber;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Submit Sepulsa Transaction
     * @param $phone
     * @param $productType
     * @param $productId
     * @param $productAmount
     * @param $agentName null
     * @param $customerEmail null
     * @return mixed
     */
    public function submitSepulsa($phone,$productType,$productId,$productAmount,$agentName = null,$customerEmail= null){
        $url = 'service/sepulsa/postTransaction';
        $param = [];
        $param['phone'] = $phone;
        $param['product_type'] = $productType;
        $param['product_id'] = $productId;
        $param['product_amount'] = $productAmount;
        $param['agent_name'] = $agentName;
        $param['customer_email'] = $customerEmail;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Submit PLN PrePaid
     * @param $phone
     * @param $productId
     * @param $productAmount
     * @param $meterNumber
     * @param $agentName null
     * @param $customerEmail null
     * @return mixed
     */
    public function submitPLNPrePaid($phone,$productId,$productAmount,$meterNumber,$agentName = null, $customerEmail= null){
        $url = 'service/sepulsa/postTransaction';
        $param = [];
        $param['phone'] = $phone;
        $param['product_type'] = 'electricity';
        $param['product_id'] = $productId;
        $param['product_amount'] = $productAmount;
        $param['meter_number'] = $meterNumber;
        $param['agent_name'] = $agentName;
        $param['customer_email'] = $customerEmail;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Submit PLN Post Paid
     * @param $phone
     * @param $productAmount
     * @param $meterNumber
     * @param $agentName
     * @param $customerEmail null
     * @return mixed
     */
    public function submitPLNPostPaid($phone,$productAmount,$meterNumber,$agentName=null, $customerEmail= null){
        $url = 'service/sepulsa/postTransaction';
        $param = [];
        $param['phone'] = $phone;
        $param['product_type'] = 'electricity_postpaid';
        $param['product_amount'] = $productAmount;
        $param['meter_number'] = $meterNumber;
        $param['agent_name'] = $agentName;
        $param['customer_email'] = $customerEmail;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * @param $phone
     * @param $bpjsNumber
     * @param $productAmount
     * @param $agentName null
     * @param $customerEmail null
     * @return mixed
     */
    public function submitBPJS($phone,$bpjsNumber,$productAmount,$agentName = null, $customerEmail= null){
        $url = 'service/sepulsa/postTransaction';
        $param = [];
        $param['phone'] = $phone;
        $param['product_type'] = 'bpjs_kesehatan';
        $param['bpjs_number'] = $bpjsNumber;
        $param['product_amount'] = $productAmount;
        $param['agent_name'] = $agentName;
        $param['customer_email'] = $customerEmail;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * @param $phone
     * @param $pdamNumber
     * @param $operatorCode
     * @param $productAmount
     * @param $agentName
     * @param $customerEmail null
     * @return mixed
     */
    public function submitPDAM($phone,$pdamNumber,$operatorCode,$productAmount,$agentName = null, $customerEmail= null){
        $url = 'service/sepulsa/postTransaction';
        $param = [];
        $param['phone'] = $phone;
        $param['product_type'] = 'pdam';
        $param['pdam_number'] = $pdamNumber;
        $param['operator_code'] = $operatorCode;
        $param['product_amount'] = $productAmount;
        $param['agent_name'] = $agentName;
        $param['customer_email'] = $customerEmail;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * @param $phone
     * @param $telkomNumber
     * @param $productAmount
     * @param $agentName null
     * @param $customerEmail null
     * @return mixed
     */
    public function submitTelkom($phone,$telkomNumber,$productAmount,$agentName = null, $customerEmail= null){
        $url = 'service/sepulsa/postTransaction';
        $param = [];
        $param['phone'] = $phone;
        $param['product_type'] = 'telkom_postpaid';
        $param['telkom_number'] = $telkomNumber;
        $param['product_amount'] = $productAmount;
        $param['agent_name'] = $agentName;
        $param['customer_email'] = $customerEmail;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * submit popshop item
     * @param null $productInfo
     * @param null $customerInfo
     * @param null $purchaseInfo
     * @param null $deliveryAddress
     * @return mixed
     */
    public function submitPopShop($productInfo=null,$customerInfo=null,$purchaseInfo=null,$deliveryAddress = null){
        $url = 'ordershop/submit';
        $param = [];
        $param['product_info'] = $productInfo;
        $param['customer_info'] = $customerInfo;
        $param['purchase_info'] = $purchaseInfo;
        $param['delivery_address'] = $deliveryAddress;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get popsend pickup province destination
     * @return mixed
     */
    public function getProvincePickupData(){
        $url = 'destination/province';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get pickup
     * @param null $province
     * @return mixed
     */
    public function getCityPickupData($province=null){
        $url = 'destination/city';
        $param = [];
        $param['province'] = $province;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Calculate PopSend
     * @param $param
     * @return mixed
     */
    public function popsendCalculation($param){
        $url = 'pickup/agentCalc';
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Submit PopSend
     * @param $param
     * @return mixed
     */
    public function popsendSubmit($param){
        $url = 'pickup/agentSubmit';
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Submit Create Virtual Account
     * @param $param
     * @return mixed
     */
    public function paymentDokuVA($param){
        $url = 'payment/dokuVA';
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }
}