<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Payment extends Model
{
    // set table
    protected $table = 'payments';

    /**
     * add Payment
     * @param $transactionRef
     * @param $username
     * @param $method
     * @param $timeLimit null
     * @return \stdClass
     */
    public static function addPayment($transactionRef,$username,$method,$timeLimit=null){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // check transaction
        $userDb = User::where('username',$username)->first();
        $transactionDb = Transaction::where('reference',$transactionRef)->where('users_id',$userDb->id)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction';
            return $response;
        }

        $module = $transactionDb->type;
        if (empty($module)) $module = 'purchase';

        // check method
        $methodDb = PaymentMethod::join('payment_available_module','payment_available_module.payment_methods_id','payment_methods.id')
            ->where('module',$module)
            ->where('code',$method)
            ->first();

        if (!$methodDb){
            $response->errorMsg = 'Invalid Method Payment';
            return $response;
        }

        // check on DB payment
        $checkDb = self::where('transactions_id',$transactionDb->id)->first();
        if ($checkDb){
            $response->errorMsg = 'Transaction Payment Already Exist';
            return $response;
        }

        // set time limit
        if (empty($timeLimit)){
            $timeLimit = date('Y-m-d H:i:s',strtotime("+3 hours"));
            if ($method=='deposit'){
                $timeLimit = date('Y-m-d H:i:s',strtotime("+1 days"));
            } elseif ($method=='transfer'){
                $timeLimit = date('Y-m-d H:i:s',strtotime("+1 days"));
            } elseif ($method=='doku_va'){
                $timeLimit = date('Y-m-d H:i:s',strtotime("+12 hours"));
            } elseif ($method=='cash'){
                $timeLimit = date('Y-m-d H:i:s', strtotime('+900 years'));
            }
        }

        // insert into payment
        $data = new self();
        $data->payment_methods_id = $methodDb->payment_methods_id;
        $data->transactions_id = $transactionDb->id;
        $data->amount = $transactionDb->total_price;
        $data->status = 'WAITING';
        $data->time_limit = $timeLimit;
        $data->save();

        // update transaction into waiting
        $transactionUpdate = Transaction::updateStatusTransaction($transactionDb->id,'WAITING');
        if (!$transactionUpdate->isSuccess){
            $response->errorMsg = 'Invalid Status';
            return $response;
        }

        $user = $userDb->name;
        $remarks = "Add Payment $method";
        $historyDb = TransactionHistory::createNewHistory($transactionDb->id,$user,'WAITING',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        $response->isSuccess = true;
        $response->paymentId = $data->id;
        return $response;
    }

    /**
     * pay payment
     * @param $transactionRef
     * @param $username
     * @return \stdClass
     */
    public static function paidPayment($transactionRef,$username,$agentPassword){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // check on db payment
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction Reference';
            return $response;
        }
        $paymentDb = self::where('transactions_id',$transactionDb->id)->first();
        if (!$paymentDb){
            $response->errorMsg = 'Transaction Not Available in Payment';
            return $response;
        }
        if ($transactionDb->status=='PAID'){
            $response->errorMsg = 'Transaction Already Paid';
            return $response;
        }
        if ($transactionDb->status=='CANCELLED'){
            $response->errorMsg = 'Transaction Already Cancelled';
            return $response;
        }

        if ($transactionDb->status=='EXPIRED'){
            $response->errorMsg = 'Transaction Already Expired';
            return $response;
        }

        if ($transactionDb->status == 'REFUND') {
            $response->errorMsg = 'Transaction Already Refunded';
            return $response;
        }

        // check time limit payment
        $timeLimit = strtotime($paymentDb->time_limit);
        $now = time();
        if ($now > $timeLimit){
            $response->errorMsg = 'Transaction Already Expired';
            return $response;
        }

        // get userDB
        $userDb = User::where('username',$username)->first();
        if ($userDb->status == 3){
            $response->errorMsg = 'Data Agent belum lengkap, silakan menghubungi CS Agent 0815-1897-889';
            return $response;
        }
        $lockerId = $userDb->locker_id;

        // checking hash password
        if (!Hash::check($agentPassword,$userDb->password)){
            $response->errorMsg = 'Kata sandi yang Anda masukan belum sesuai.';
            return $response;
        }

        // get method payment
        $method = $paymentDb->method->code;
        $isPaid = false;
        $amount = $paymentDb->amount;
        $transactionId = $transactionDb->id;
        switch ($method){
            case 'deposit' :
                $deduct = self::deductDeposit($lockerId,$amount,$transactionId);
                if ($deduct->isSuccess) $isPaid = true;
                else $response->errorMsg = $deduct->errorMsg;
                break;
            case 'cash' :
                $isPaid = true;
                break;
            default :
                $isPaid = false;
        }

        if ($isPaid==false && $method !== 'cash'){
            if (empty($response->errorMsg)) $response->errorMsg = 'Failed Payment';
            return $response;
        }

        if ($method !== 'cash'){
            // update status to paid
            $data = self::find($paymentDb->id);
            $data->status = 'PAID';
            $data->save();
    
            $user = $userDb->name;
            $remarks = "Paid Payment $method";
            $historyDb = TransactionHistory::createNewHistory($transactionDb->id,$user,'PAID',$remarks);
            if (!$historyDb->isSuccess){
                $response->errorMsg = "Failed Create Transaction History";
                return $response;
            }
    
            // update transaction
            $transactionDb = Transaction::updateStatusTransaction($transactionDb->id,'PAID');
            if (!$transactionDb->isSuccess){
                $response->errorMsg = $transactionDb->errorMsg;
                return $response;
            }
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * get payment detail
     * @param $transactionRef
     * @return \stdClass
     */
    public static function detailPayment($transactionRef){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        // find transaction DB
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        // get payment
        $paymentDb = self::where('transactions_id',$transactionDb->id)->first();
        if (!$paymentDb){
            $response->errorMsg = 'Transaction Not Found in Payment';
            return $response;
        }

        $data = new \stdClass();
        $data->agent = $transactionDb->user->name;
        $data->reference = $transactionDb->reference;
        $data->description = $transactionDb->description;
        $data->total_price = $transactionDb->total_price;
        $data->status = $paymentDb->status;
        $data->payment_method = $paymentDb->method->code;

        $response->isSuccess = true;
        $response->data = $data;

        return $response;
    }

    /**
     * create commission transaction
     * @param $transactionRef
     * @return \stdClass
     */
    public static function createCommissionTransaction($transactionRef){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // find transaction DB
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        // get transaction item
        $transactionItems = $transactionDb->items;
        $commissionTotal = 0;
        // calculate commission
        foreach ($transactionItems as $transactionItem) {
            $commissionId = $transactionItem->commission_schemas_id;
            $basicPrice = $transactionItem->price;
            $publishPrice = $transactionItem->price;

            if (empty($commissionId)) continue;

            $schemaDb = CommissionSchema::calculateById($commissionId,$basicPrice,$publishPrice);
            if (!$schemaDb->isSuccess){
                $response->errorMsg = $schemaDb->errorMsg;
                return $response;
            }
            $commissionTotal += $schemaDb->commission;
        }

        if ($commissionTotal==0){
            $response->isSuccess = true;
            return $response;
        }

        // get userDb
        $userId = $transactionDb->users_id;
        $userDb = User::find($userId);

        // get locker DB
        $lockerDb = Locker::find($userDb->locker_id);
        if (!$lockerDb){
            $response->errorMsg = 'Locker Agent not Found';
            return $response;
        }

        // get status
        $status = $lockerDb->status_verification;
        if ($status == 'verified'){
            // create new transaction
            $commissionTransaction = new Transaction();
            $commissionTransaction->users_id = $transactionDb->users_id;
            $commissionTransaction->transaction_id_reference = $transactionDb->id;
            $commissionTransaction->type = 'commission';
            $transactionReference = 'TRX-'.$transactionDb->users_id.date('ymdhi').Helper::generateRandomString(3);
            $commissionTransaction->reference = $transactionReference;
            $commissionTransaction->description = "Commission from $transactionDb->reference at".date('Y-m-d H:i:s');
            $commissionTransaction->total_price = $commissionTotal;
            $commissionTransaction->status = 'PAID';
            $commissionTransaction->save();

            // top up deposit
            $depositDb = self::creditDeposit($transactionDb->user->locker_id,$commissionTotal,$commissionTransaction->id);
            if (!$depositDb->isSuccess){
                $response->errorMsg = $depositDb->errorMsg;
                return $response;
            }
        } else {
            $numberOfExpiredDate = 14;
            $expiredDate = date('Y-m-d H:i:s',strtotime("+$numberOfExpiredDate days"));

            // insert into pending transaction
            $pendingTransaction = new PendingTransaction();
            $pendingTransaction->user_id = $transactionDb->users_id;
            $pendingTransaction->transaction_id_reference = $transactionDb->id;
            $pendingTransaction->type = 'commission';
            $transactionReference = 'TRX-'.$transactionDb->users_id.date('ymdhi').Helper::generateRandomString(3);
            $pendingTransaction->reference = $transactionReference;
            $pendingTransaction->description = "Commission from $transactionDb->reference at".date('Y-m-d H:i:s');
            $pendingTransaction->total_price = $commissionTotal;
            $pendingTransaction->status = 'PENDING';
            $pendingTransaction->expired_date = $expiredDate;
            $pendingTransaction->save();
        }

        $response->isSuccess = true;
        return $response;
    }

    /*Private function*/
    /**
     * deduct deposit
     * @param $lockerId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    private static function deductDeposit($lockerId,$amount,$transactionId){
        // generate standard response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get lockerId Deposit
        $lockerDb = Locker::find($lockerId);
        if (!$lockerDb){
            $response->errorMsg = 'Invalid Locker';
            return $response;
        }
        $currentBalance = $lockerDb->balance;
        if ($currentBalance < $amount){
            $response->errorMsg = 'Deposit Anda tidak cukup untuk melakukan transaksi.';
            return $response;
        }
        // deduct deposit
        $newBalance = $currentBalance - $amount;

        // update to locker balance
        $lockerDb->balance = $newBalance;
        $lockerDb->save();

        // insert into balance record
        $credit = 0;
        $debit = $amount;
        $balanceRecordDb = BalanceRecord::insertNewRecord($lockerId,$transactionId,$credit,$debit,$newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /**
     * credit / top up deposit
     * @param $lockerId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public static function creditDeposit($lockerId,$amount,$transactionId){
        // generate standard response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get lockerId Deposit
        $lockerDb = Locker::find($lockerId);
        if (!$lockerDb){
            $response->errorMsg = 'Invalid Locker';
            return $response;
        }
        $currentBalance = $lockerDb->balance;
        // credit deposit
        $newBalance = $currentBalance + $amount;

        // update to locker balance
        $lockerDb->balance = $newBalance;
        $lockerDb->save();

        // insert into balance record
        $credit = $amount;
        $debit = 0;
        $balanceRecordDb = BalanceRecord::insertNewRecord($lockerId,$transactionId,$credit,$debit,$newBalance);

        $response->isSuccess = true;
        return $response;
    }
    
    /**
     * pay payment
     * @param $transactionRef
     * @param $username
     * @return \stdClass
     */
    public static function paidPaymentByNonVA($transactionRef, $username){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }
        
        // check on db payment
        $transactionDb = Transaction::where('reference', $transactionRef)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction Reference';
            return $response;
        }
        $paymentDb = self::where('transactions_id', $transactionDb->id)->first();
        if (!$paymentDb){
            $response->errorMsg = 'Transaction Not Available in Payment';
            return $response;
        }
//         if ($transactionDb->status=='PAID'){
//             $response->errorMsg = 'Transaction Already Paid';
//             return $response;
//         }
        if ($transactionDb->status=='CANCELLED'){
            $response->errorMsg = 'Transaction Already Cancelled';
            return $response;
        }
        
//         if ($transactionDb->status=='EXPIRED'){
//             $response->errorMsg = 'Transaction Already Expired';
//             return $response;
//         }
        
        if ($transactionDb->status == 'REFUND') {
            $response->errorMsg = 'Transaction Already Refunded';
            return $response;
        }
        
        // update status to paid
        $data = self::find($paymentDb->id);
        if($data){
            $data->status = 'PAID';
            $data->save();
        }
        
        $remarks = 'Paid Payment '.$paymentDb->method->code;
        $historyDb = TransactionHistory::createNewHistory($transactionDb->id, $userDb->name, 'PAID', $remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }
        
        // update transaction
        $transactionDb = Transaction::updateStatusTransaction($transactionDb->id, 'PAID');
        if (!$transactionDb->isSuccess){
            $response->errorMsg = $transactionDb->errorMsg;
            return $response;
        }
        
        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function method(){
        return $this->belongsTo(PaymentMethod::class,'payment_methods_id','id');
    }

    public function transaction(){
        return $this->belongsTo(Transaction::class,'transactions_id','id');
    }

    public function transfer(){
        return $this->belongsTo(PaymentTransfer::class,'payments_id','id');
    }

    public function dokuva(){
        return $this->hasOne(PaymentDokuVA::class,'payments_id','id');
    }
}
