<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryDistrict extends Model
{
    protected $table = 'delivery_districts';

    /*relationship*/
    public function city(){
        return $this->belongsTo(DeliveryCity::class,'delivery_city_id','id');
    }
}
