<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PendingTransaction extends Model
{
    protected $table = 'pending_transactions';

    public static function createPendingTransaction($username,$type,$reference,$description,$amount,$transactionIdReference = null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        $typeList = ['commission','referral','reward'];
        if (!in_array($type,$typeList)){
            $response->errorMsg = 'Invalid Pending Type';
            return $response;
        }

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }

        $numberOfExpiredDate = 14;
        $expiredDate = date('Y-m-d H:i:s',strtotime("+$numberOfExpiredDate days"));

        // insert into pending transaction
        $pendingTransaction = new PendingTransaction();
        $pendingTransaction->user_id = $userDb->id;
        $pendingTransaction->transaction_id_reference = $transactionIdReference;
        $pendingTransaction->type = $type;
        $transactionReference = $reference;
        $pendingTransaction->reference = $transactionReference;
        $pendingTransaction->description = $description;
        $pendingTransaction->total_price = $amount;
        $pendingTransaction->status = 'PENDING';
        $pendingTransaction->expired_date = $expiredDate;
        $pendingTransaction->save();

        $response->isSuccess = true;
        $response->transactionId = $pendingTransaction->id;
        return $response;
    }

    /*Relationship*/
    public function user(){
        return $this->belongsTo(User::class);
    }
}
