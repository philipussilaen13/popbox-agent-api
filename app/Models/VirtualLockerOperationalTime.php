<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerOperationalTime extends Model
{
    // set table and connection
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_operational_times';
}
