<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    // set table
    protected $table = 'carts';

    /*============ Public function ============*/

    /**
     * get cart items / detail
     * @param $username
     * @return \stdClass
     */
    public static function getCarts($username){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        $response->cartId = null;

        // get cart DB
        $cartDb = User::where('username',$username)->first()->carts()->where('status',1)->first();
        if (empty($cartDb)){
            $response->errorMsg = 'Cart Not Exist / Empty';
            return $response;
        }
        // get cart items
        $itemsDb = self::find($cartDb->id)->cartItems;

        // generate data
        $data = new \stdClass();
        $data->cart_reference = $cartDb->reference;
        $data->created_at = date('Y-m-d H:i:s',strtotime($cartDb->created_at));
        $data->total_price = 0;
        $data->total_commission = 0;
        $items = [];
        $totalPrice = 0;
        $totalCommission = 0;
        foreach ($itemsDb as $item) {
            $tmp = new \stdClass();
            $tmp->id = $item->id;
            $tmp->name = $item->name;
            $tmp->type = $item->type;
            $tmp->picture = $item->picture;
            $tmp->params = $item->params;
            $tmp->price = $item->price;
            $tmp->commission = 0;
            $tmp->added_at = date('Y-m-d H:i:s',strtotime($item->created_at));

            // calculate commission
            $basicPrice = $item->price;
            $publishPrice = $item->price;
            $schema = CommissionSchema::calculateSchema($item->type,$basicPrice,$publishPrice);
            if ($schema->isSuccess){
                $tmp->commission = $schema->commission;
            }
            $items[] = $tmp;
            $totalPrice+= $item->price;
            $totalCommission+= $tmp->commission;
        }

        $data->items = $items;
        $data->total_price = $totalPrice;
        $data->total_commission = $totalCommission;
        $response->isSuccess = true;
        $response->data = $data;
        $response->cartId = $cartDb->id;

        return $response;
    }

    /** create cart
     * @param $username
     * @return \stdClass
     */
    public static function createCart($username){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->cartId = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        // create reference
        $reference = "CRT-".$userDb->id.'-'.date('ymd');

        // insert to DB
        $data = new self();
        $data->users_id = $userDb->id;
        $data->reference = $reference;
        $data->status = 1;
        $data->save();

        // response
        $response->isSuccess = true;
        $response->cartId = $data->id;
        return $response;
    }

    /**
     * remove cart
     * @param $username
     * @return \stdClass
     */
    public static function removeCart($username){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        $response->cartId = null;

        // get cart DB
        $userDb = User::where('username',$username)->first();
        if (empty($userDb)){
            $response->errorMsg = 'Cart Not Exist';
            return $response;
        }
        // update to 0
        self::where('status',1)
            ->where('users_id',$userDb->id)
            ->update(['status' => 0]);

        $response->isSuccess = true;
        return $response;
    }


    /*========== End Public function ==========*/

    /* Relationship */

    public function user(){
        return $this->belongsTo(User::class,'users_id','id');
    }

    public function cartItems(){
        return $this->hasMany(CartItem::class,'carts_id','id');
    }
}
