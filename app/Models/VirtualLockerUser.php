<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerUser extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'users';

    public static function insertAgentUser($input){
        //generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $name = 'Agent '.$input['name'];
        $phone = $input['phone'];
        $email = empty($input['email']) ? null : $input['email'];
        $username ='agent_'.$input['phone'];
        $groupName = 'agent';
        $companyCode = 'POPBOX';

        // get groupDb
        $groupDb = VirtualLockerGroup::where('name',$groupName)->first();
        if(!$groupDb){
            $response->errorMsg = 'Invalid Group Name';
            return $response;
        }
        // get companyDb
        $companyDb = VirtualLockerCompany::where('prefix',$companyCode)->first();
        if(!$companyDb){
            $response->errorMsg = 'Invalid Company';
            return $response;
        }

        // checking username
        $check = self::where('phone',$phone)->first();
        if ($check){
            $response->errorMsg = 'Phone Already Taken';
            return $response;
        }

        // insert to DB

        $userDb = new self();
        $userDb->name = $name;
        $userDb->phone = $phone;
        $userDb->email = $email;
        $userDb->username = $username;
        $userDb->groups_id = $groupDb->id;
        $userDb->companies_id = $companyDb->id;
        $userDb->save();

        $response->isSuccess = true;
        $response->id = $userDb->id;

        return $response;
    }
}
