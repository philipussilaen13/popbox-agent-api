<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ErrorDetail extends Model
{
    // set table
    protected $table = 'error_details';
}
