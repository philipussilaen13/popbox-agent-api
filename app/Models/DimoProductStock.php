<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DimoProductStock extends Model
{
    // set connection
    protected $connection = 'popbox_db';
    // set table
    protected $table = 'dimo_product_stock';
}
