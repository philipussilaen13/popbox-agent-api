<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locker extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_virtual';
    protected $table = 'lockers';
    
    /**
     * @param $regionCode
     * @return mixed
     */
    public static function getLockerByRegionCode($regionCode)
    {
        $locker = self::where('region_code', $regionCode)->first();
        
        return $locker;
    }
    
    /**
     * @param $lockerId
     * @return mixed
     */
    public static function getLockerByLockerId($lockerId)
    {
        $locker = self::where('locker_id', $lockerId)->first();
        
        return $locker;
    }

}
