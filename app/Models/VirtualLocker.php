<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class VirtualLocker extends Model
{
    // define connection
    protected $connection = 'popbox_virtual';
    protected $table = 'lockers';

    /**
     * Insert new Locker
     * @param $input
     * @return \stdClass
     */
    public static function insertLocker($input){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->id = null;
        $response->lockerId = null;

        $lockerName = strtoupper($input['locker_name']);
        $address = $input['address'];

        // cleanup special char
        $lockerName = preg_replace('/[^A-Za-z0-9]/', ' ',$lockerName);

        // get city id
        $cityDb = VirtualLockerCity::where('id',$input['city_id'])->first();
        if (!$cityDb){
            $response->errorMsg = 'City Not Found';
            return $response;
        }
        $cityId = $cityDb->id;

        // validate service
        $services = $input['services'];
        if (!is_array($services)){
            $response->errorMsg = 'Services must be array';
            return $response;
        }

        $servicesDb = VirtualLockerListService::whereIn('code',$services)->get();
        if ($servicesDb->count()==0){
            $response->errorMsg = "Invalid / No Selected Service";
            return $response;
        }

        // validate days, open_hour, close hour
        $days = $input['days'];
        $openHour = $input['open_hour'];
        $closeHour = $input['close_hour'];
        if (!is_array($days)){
            $response->errorMsg = 'Days must be array';
            return $response;
        }
        if (!is_array($openHour)){
            $response->errorMsg = 'Open Hour must be array';
            return $response;
        }
        if (!is_array($closeHour)){
            $response->errorMsg = 'Close Hour must be array';
            return $response;
        }

        // validate and generate 7 items (if not) for days, open_hour, close_hour
        if (count($days)<7){
            $diff = 7 - count($days); #different between 7 items and current items in days array
            $end = count($days); #get last item in days array
            for ($i=0;$i<$diff;$i++){
                $days[] = $end+$i;
            }
        }
        if (count($openHour)<7){
            $diff = 7 - count($openHour); #different between 7 items and current items in open hour array
            for ($i=0;$i<$diff;$i++){
                $openHour[] = null;
            }
        }
        if (count($closeHour)<7){
            $diff = 7 - count($closeHour); #different between 7 items and current items in close hour array
            for ($i=0;$i<$diff;$i++){
                $closeHour[] = null;
            }
        }

        // generate lockerId
        $provinceCode = $cityDb->province->province_code;
        $randomString = Helper::generateRandomString(10);
        // check lockerId
        $isExist = true;
        while ($isExist){
            $lockerId = self::generateLockerId($provinceCode,$randomString);
            $check = self::where('locker_id',$lockerId)->first();
            if (!$check) $isExist = false;
        }

        $detailAddress = (!empty($input['detail_address'])) ? $input['detail_address'] : null;
        $picture = empty($input['picture']) ? null : $input['picture'];
        $status = 0;
        $lat = empty($input['latitude']) ? null : $input['latitude'];
        $long = empty($input['longitude']) ? null : $input['longitude'];

        //insert to DB
        $lockerDb = new self();
        $lockerDb->locker_id = $lockerId;
        $lockerDb->locker_name = $lockerName;
        $lockerDb->address = $address;
        $lockerDb->cities_id = $cityId;
        $lockerDb->latitude = $lat;
        $lockerDb->longitude = $long;
        $lockerDb->detail_address = $detailAddress;
        
        // saving picture
        if (!empty($picture)){
            $filename = $lockerId.'.jpg';
//             $server =  $_SERVER['DOCUMENT_ROOT'];
//             $path = $server.env('LOCKER_LOC')."/img/locker/$filename";
//             $img = base64_decode($picture);
//             $saving = file_put_contents($path,$img);
//             if (!$saving){
//                 $response->errorMsg = "Failed to Save Picture";
//                 return $response;
//             }
            Storage::disk('storePict')->put($filename, base64_decode($picture));
            $lockerDb->picture = $filename;
        }
        $lockerDb->status = $status;
        $lockerDb->save();

        $response->id = $lockerDb->id;
        $response->lockerId = $lockerId;

        // if isset user_name
        $username = empty($input['username']) ? null : $input['username'];
        if (!empty($username)){
            // get uer data
            $userDb = VirtualLockerUser::where('username',$username)->first();
            $userDb->lockers_id = $lockerDb->id;
            $userDb->save();
        }

        // insert into locker_service
        foreach ($servicesDb as $item){
            $data = new VirtualLockerService();
            $data->list_locker_services_id = $item->id;
            $data->lockers_id = $lockerDb->id;
            $data->save();
        }

        // insert into locker operational time
        foreach ($days as $index => $item){
            $data = new VirtualLockerOperationalTime();
            $data->lockers_id = $lockerDb->id;
            $data->day = $days[$index];
            $data->open_hour = $openHour[$index];
            $data->close_hour = $closeHour[$index];
            $data->save();
        }

        $response->isSuccess = true;
        return $response;
    }

    public static function generateLockerId($provinceCode = null,$randomString = null){
        $nowDate = date('ymd');
        $lockerId = $provinceCode.$nowDate.$randomString;
        return $lockerId;
    }

    /*Relationship*/

    public function lockerStatus(){
        return $this->hasMany(VirtualLockerStatus::class,'id','lockers_id');
    }

    public function operational_time(){
        return $this->hasMany(VirtualLockerOperationalTime::class,'lockers_id','id');
    }

    public function services(){
        return $this->belongsToMany(VirtualLockerListService::class,'locker_services','lockers_id','list_locker_services_id');
    }

    public function city(){
        return $this->belongsTo(VirtualLockerCity::class,'cities_id','id');
    }
}
