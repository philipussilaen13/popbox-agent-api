<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerCompany extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'companies';
}
