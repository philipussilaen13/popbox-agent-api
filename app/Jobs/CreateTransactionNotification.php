<?php

namespace App\Jobs;

use App\Models\Transaction;
use App\Models\VirtualLocker;
use App\Models\VirtualLockerWebNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateTransactionNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transactions = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Transaction $transactions)
    {
        $this->transactions = $transactions;
        $this->uniqueId = uniqid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $transactionDb = $this->transactions;
        $totalPrice = $transactionDb->total_price;
        $items = $transactionDb->items;

        $lockerName = null;
        $lockerDb = VirtualLocker::where('locker_id',$transactionDb->user->locker_id)->first();
        if ($lockerDb){
            $lockerName = $lockerDb->locker_name;
        } else {
           $lockerName = $transactionDb->user->locker_id;
        }

        $listItem = [];
        foreach ($items as $item){
            $listItem[] = $item->name;
        }
        $items = implode(',',$listItem);
        $webNotif = new VirtualLockerWebNotification();
        $content = "Sukses Transaksi $lockerName Total : $totalPrice. Item : $items ";
        $webNotif->createNotification('transaction',"groups",'1','success',$content, null);
        $webNotif->createNotification('transaction',"groups",'6','success',$content, null);
        $webNotif->createNotification('transaction',"groups",'7','success',$content, null);
        $webNotif->createNotification('transaction',"groups",'8','success',$content, null);
    }
}
