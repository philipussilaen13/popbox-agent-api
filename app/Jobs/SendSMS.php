<?php

namespace App\Jobs;

use App\Models\SmsNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $uniqueId = null;
    // function from old app
    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $is_post = 0;
    protected $smsNotification = null;

    /**
     * Create Jobs Instance
     * SendSMS constructor.
     * @param SmsNotification $smsNotification
     */
    public function __construct(SmsNotification $smsNotification)
    {
        $this->smsNotification = $smsNotification;
        $this->uniqueId = uniqid();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $smsNotification = $this->smsNotification;
        // get destination
        $phone = $smsNotification->to;
        // get vendor
        $vendor = $smsNotification->vendor;
        // get sms content
        $sms = $smsNotification->sms;

        // send sms
        if ($vendor=='jatis'){
            $response = $this->sendJatis($phone,$sms);
            $smsNotification->status = 1;
        }
        elseif ($vendor=='twilio'){
            $response = $this->sendTwilio($phone,$sms);
            $smsNotification->status = 1;
        }
        elseif ($vendor=='nexmo'){
            $response = $this->sendNexmo($phone,$sms);
            $smsNotification->status = 1;
        }
        else {
            $response = $this->sendGlobal($phone,$sms);
            $smsNotification->status = 1;
        }

        $smsNotification->save();
    }

    private function sendJatis($phone,$message){
        $sms = json_encode(['to' => $phone,
            'message' => $message,
            'token' => '0weWRasJL234wdf1URfwWxxXse304'
        ]);
        $urlsms = "http://smsdev.popbox.asia/sms/send";

        //logging
        $log = "$urlsms : $sms";
        $this->log($log);
        $result = $this->post_data($urlsms,$sms);

        $tmp = json_encode($result);
        //logging
        $this->log($tmp);
        return;
    }

    private function sendNexmo($phone,$message){
        $sms = json_encode(['to' => $phone,
            'message' => $message,
            'token' => '0weWRasJL234wdf1URfwWxxXse304'
        ]);
        $urlsms = "http://smsdev.popbox.asia/sms/send";

        //logging
        $log = "$urlsms : $sms";
        $this->log($log);
        $result = $this->post_data($urlsms,$sms);

        $tmp = json_encode($result);
        //logging
        $this->log($tmp);
        return;
    }

    private function sendTwilio($phone,$message){
        $phone = substr($phone, 1);
        $phone = "+62".$phone;

        $sms = json_encode(['to' => $phone,
            'message' => $message,
            'token' => '2349oJhHJ20394j2LKJO034823423'
        ]);
        $urlsms = "http://smsdev.popbox.asia/sms/send/tw";
        //logging
        $log = "$urlsms : $sms";
        $this->log($log);

        $result = $this->post_data($urlsms,$sms);

        //logging
        $tmp = json_encode($result);
        $this->log($tmp);
        return;
    }

    private function sendGlobal($phone,$message){
        $sms = json_encode(['to' => $phone,
            'message' => $message,
            'token' => '0weWRasJL234wdf1URfwWxxXse304'
        ]);
        $urlSMS = "https://internalapi.popbox.asia/sms/send";

        //logging
        $log = "$urlSMS : $sms";
        $this->log($log);
        $result = $this->post_data($urlSMS,$sms);

        $tmp = json_encode($result);
        //logging
        $this->log($tmp);
        return;
    }

    /**
     * logging
     * @param $msg
     */
    private function log($msg){
        $uniqueId = $this->uniqueId;
        $msg = "SMS $uniqueId $msg\n";
        $f = fopen(storage_path().'/logs/apps/'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
        return;
    }

    /**
     * Post data to third party apps
     * @param $url
     * @param array $post_data
     * @param array $headers
     * @param array $options
     * @return mixed|null
     */
    private function post_data($url, $post_data = array(), $headers = array(), $options = array()) {
        $result = null;
        $curl = curl_init ();

        if ((is_array ( $options )) && count ( $options ) > 0) {
            $this->options = $options;
        }
        if ((is_array ( $headers )) && count ( $headers ) > 0) {
            $this->headers = $headers;
        }
        if ($this->is_post !== null) {
            $this->is_post = 1;
        }

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLOPT_POST, $this->is_post );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $post_data );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $this->headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );
        $result = json_decode ( $content, TRUE );

        curl_close ( $curl );
        return $result;
    }
}
