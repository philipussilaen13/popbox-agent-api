<?php
/**
 * User: faisal
 * Date: 11/12/2018
 * Time: 14.34
 */

return [
    'popwarung' => [
        'api_url' => env('WARUNG_API_URL' ),
    ],
    'report' => [
        'url' => env('REPORT_URL', '<base>.popbox.asia' ),
    ]
];