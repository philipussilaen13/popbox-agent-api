<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('auth/activation/{id}','UserController@activationAgent');
Route::group(['prefix'=>'auth'],function(){
    Route::get('reset/{code}','UserController@getResetPage');
    Route::post('reset','UserController@postResetPassword');
    Route::get('agreement','UserController@getAgreementPage');
});

Route::post('paymentCallback','PaymentController@paymentCallback');
Route::post('paymentOpenCallback','PaymentController@paymentOpenCallback');

Route::post('transactionItemCallback','TransactionController@callbackTransactionItem');
Route::post('deliveryCallback','DeliveryController@deliveryCallback');

Route::get('run/{password}',function ($password){
   if ($password!='developerGanteng') return 'GAK BOLEH NAKAL';
   \Illuminate\Support\Facades\Artisan::call('referral:transaction');
});
Auth::routes();


Route::middleware(['auth'])->group(function () {
    // LOGS API
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('changeCategory', 'CategoryPopshopController@changeListCategory');
