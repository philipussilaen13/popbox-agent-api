<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_order_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_order_id',false,true);
            $table->string('status',100);
            $table->text('remarks');
            $table->timestamps();

            $table->foreign('delivery_order_id')->references('id')->on('delivery_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_histories');
    }
}
