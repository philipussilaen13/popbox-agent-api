<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerificationPerImageColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lockers', function (Blueprint $table) {
            $table->dropColumn('verification_message');
            $table->string('id_verification',256)->nullable()->after('status_verification');
            $table->string('selfie_verification',256)->nullable()->after('id_verification');
            $table->string('store_verification',256)->nullable()->after('selfie_verification');
            $table->string('npwp_verification',256)->nullable()->after('store_verification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lockers', function (Blueprint $table) {
            $table->string('verification_message',256)->nullable()->after('status_verification');
            $table->dropColumn('id_verification');
            $table->dropColumn('selfie_verification');
            $table->dropColumn('store_verification');
            $table->dropColumn('npwp_verification');
        });
    }
}
