<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
			<title>Excel To HTML using codebeautify.org</title>
		</head>
		<body>
			<p align="center">
				<strong>PERJANJIAN KEMITRAAN</strong>
				<strong></strong>
			</p>
			<p align="center">
				<strong></strong>
			</p>
			<p align="justify">
    1. 
				
				<strong>LINGKUP KERJA SAMA</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    Para Pihak sepakat bekerjasama dalam menyediakan layanan kepada konsumen
    PopBox dimana:
</p>
			<p align="justify">
    a. PopBox menyewakan kepada Mitra suatu aplikasi yang dapat digunakan untuk
    jasa logistik, belanja online, pembayaran dan lainnya; dan
</p>
			<p align="justify">
    b. Mitra menyediakan Lokasi dan jasa untuk menyediakan layanan kepada
    konsumen PopBox, dan untuk itu, Mitra akan memperoleh Bagi Hasil atas
    pembayaran yang diterima PopBox terkait dengan layanan Popbox Loker
    tersebut (
				
				<strong>“Bagi Hasil”</strong>) berdasarkan Perjanjian ini.

			
			</p>
			<p align="justify">
    2. 
				
				<strong>MASA PERJANJIAN</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
2.1. Perjanjian ini berlaku selama 3 (tiga) bulan sejak Tanggal Efektif (    
				
				<strong>“Masa Perjanjian”</strong>) kecuali berakhir lebih dahulu
    berdasarkan ketentuan Perjanjian ini

			
			</p>
			<p align="justify">
    2.2. Dalam waktu paling lambat 30 hari sebelum berakhirnya Masa Perjanjian,
    Para Pihak dapat memperpanjang atau menghentikan Masa Perjanjian dengan
    penyesuaian syarat dan ketentuan yang ditentukan oleh PopBox
</p>
			<p align="justify">
    3. 
				
				<strong>HAK DAN KEWAJIBAN MITRA</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    3.1. 
				
				<strong>Mitra berkewajiban:</strong>
				<strong></strong>
			</p>
			<p align="justify">
a. membayar 
				
				<strong>Deposit Awal</strong> kepada PopBox sebesar
				
				<strong>
					<em></em>
				</strong>
				<strong>Rp. 250.000,- (Dua Ratus Lima Puluh Ribu Rupiah)</strong> (    
				
				<strong>“Deposit Awal”</strong>) setelah Para Pihak menandatangani
    Perjanjian;

			
			</p>
			<p align="justify">
    b. mengelola Aplikasi Popbox Agen untuk penyediaan layanan kepada konsumen
    PopBox dengan menggunakan (i) nama dan tanda dagang PopBox, dan (ii) Sistem
    Pemasaran dan Manajemen serta Standar Operasional dan Prosedur Layanan dan
Penggunaan Popbox Loker yang diberlakukan oleh PopBox dari waktu ke waktu (    
				
				<strong>“Sistem dan Standar”</strong>);

			
			</p>
			<p align="justify">
    c. selalu menjaga kerahasiaan Sistem dan Standar dan tidak akan
    mengungkapkannya kepada pihak ketiga manapun selama dan seterusnya setelah
    berakhirnya Masa Perjanjian;
</p>
			<p align="justify">
    d. mengikatkan diri secara ekslusif dengan PopBox dalam penyediaan layanan
    Popbox Loker dan tidak akan bekerja sama dalam bentuk apapun dengan pihak
    ketiga untuk menyediakan layanan loker penyimpanan barang lain selama Masa
    Perjanjian;
</p>
			<p align="justify">
    e. selalu menjaga reputasi bisnis PopBox dan mencegah segala bentuk
    tindakan yang dapat mencemarkan reputasi tersebut selama dan seterusnya
    setelah berakhirnya Masa Perjanjian;
</p>
			<p align="justify">
    f. menyediakan Lokasi Popbox Loker dengan biayanya sendiri dan dengan
    kondisi Lokasi yang dapat diterima oleh PopBox, serta tidak akan
    memindahkan Lokasi Popbox Loker tanpa persetujuan tertulis terlebih dahulu
    dari PopBox;
</p>
			<p align="justify">
    g. memastikan dan menjaga Aplikasi Popbox online dan beroperasi penuh
    sesuai dengan perjanjian waktu buka, termasuk namun tidak terbatas untuk
    memastikan ketersediaan aliran listrik dan koneksi internet yang diperlukan
    bagi operasional Popbox Loker setiap waktu;
</p>
			<p align="justify">
    h. meminta persetujuan secara tertulis dari PopBox paling sedikit 3 (tiga)
    hari sebelum dilakukannya perubahan waktu operasional Popbox Loker
    dikarenakan oleh suatu sebab yang dapat diterima oleh PopBox dengan
    ketentuan penghentian operasional Popbox Loker tidak lebih dari 2 (dua)
    hari.;
</p>
			<p align="justify">
    i. memberikan akses kepada kurir atau konsumen yang memiliki otorisasi dari
    PopBox untuk mengambil/mengirimkan barang ke Mitra, serta kepada tim
    lapangan PopBox untuk melakukan pemeliharaan ataupun survey;
</p>
			<p align="justify">
    j. mengganti seluruh biaya yang dikeluarkan PopBox untuk memperbaiki
    kerusakan tablet yang diberikan yang diakibatkan oleh kelalaian Mitra
</p>
			<p align="justify">
    k. mempromosikan keberadaan PopBox Loker di Lokasi yang bersangkutan, baik
    secara lisan maupun dengan memberikan tanda jalan ke arah Lokasi Popbox;
    dan
</p>
			<p align="justify">
    l. memberikan pelatihan kepada personel Mitra atau personil toko di Lokasi
    Popbox Loker agar mampu memberikan penjelasan secara seksama kepada
    konsumen tentang cara penggunaan Aplikasi Popbox
</p>
			<p align="justify">
    m. menjaga barang-barang yang dikirimkan melalui Mitra agar dalam kondisi
    baik sampai barang tersebut diambil atau dikirimkan ke tujuan akhirnya; dan
    bertanggung jawab apabila terjadi kehilangan/kerusakkan pada barang dengan
    melakukan pergantian rugi sebesar sebesar harga barang
</p>
			<p align="justify">
    3.2. 
				
				<strong>Mitra berhak:</strong>
				<strong></strong>
			</p>
			<p align="justify">
    a. memperoleh Bagi Hasil dari PopBox atas penerimaan dari penggunaan Popbox
    Loker dengan perhitungan sebagai berikut:
</p>
			<p align="justify">
				<strong>I.</strong>
    Transaksi Virtual Locker : Rp. 1.000,-/trx

			
			</p>
			<p align="justify">
    II.Transaksi Pulsa : 0 – 100.000 : 10%
</p>
			<p align="justify">
    :100.000 – 250.000 : 7,5%
</p>
			<p align="justify">
    : 250.000 : 5%
</p>
			<p align="justify">
    III. Transaksi Tagihan : Rp. 2.000,-/trx
</p>
			<p align="justify">
    IV. Transaksi Belanja : 2,5%
</p>
			<p align="justify">
				<strong> b. </strong>
    menggunakan nama dan merek dagang ‘
				
				<strong>POPBOX ASIA’ </strong>sebatas
untuk pelaksanaan Lingkup Kerja Sama berdasarkan Perjanjian ini.    
				
				<strong></strong>
			</p>
			<p align="justify">
    3.3. Dalam Pasal ini yang dimaksud “Tanggal Mulai Operasi Popbox Loker”
    adalah tanggal dimana Popbox Loker telah berada di lokasi Mitra dan siap
    untuk digunakan.
</p>
			<p align="justify">
    3.4. Pembayaran hak Mitra berdasarkan Pasal 3.2 Perjanjian dilakukan secara
    otomatis ke dalam aplikasi dan dapat dicairkan apabila PopBox atau Mitra
    menentukan untuk menghentikan kerjasama dan dapat dicairkan dengan cara
    ditransfer ke rekening Mitra dan mengembalikan tablet yg diberikan ketika
    dimulai dalam kondisi baik
</p>
			<p align="justify">
    4. 
				
				<strong>HAK DAN KEWAJIBAN POPBOX</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    4.1. 
				
				<strong>PopBox berkewajiban:</strong>
				<strong></strong>
			</p>
			<p align="justify">
    a. melakukan survey lokasi dan program pelatihan kepada Mitra terkait
    layanan dan penggunaan Popbox Loker;
</p>
			<p align="justify">
    b. memastikan Popbox Loker dapat bekerja dengan baik dan benar setiap saat,
    tanpa mengurangi kewajiban Mitra berdasarkan Klausul 3.1.g;
</p>
			<p align="justify">
    c. mengatasi gangguan teknis yang menyebabkan Popbox Loker tidak dapat
    beroperasi, dalam waktu 24 (dua puluh empat) jam sejak adanya pemberitahuan
    dari Mitra;
</p>
			<p align="justify">
    4.2. 
				
				<strong>PopBox berhak:</strong>
				<strong></strong>
			</p>
			<p align="justify">
    a. mengiklankan merk dagang pihak ketiga di Popbox Loker;
</p>
			<p align="justify">
    b. menahan atau mengurangi pembayaran Bagi Hasil kepada Mitra, dalam hal
    Mitra melakukan pelanggaran atau kelalaian atas setiap kewajibannya
    berdasarkan Perjanjian ini sampai dengan Mitra dapat memperbaiki
    pelanggaran atau kelalaian tersebut dengan hasil yang dapat diterima oleh
    PopBox;
</p>
			<p align="justify">
    c. sewaktu-waktu melakukan perubahan/pengkinian (
				
				<em>update</em>) atas nama
    dan tanda dagang PopBox, lingkup produk dan layanan PopBox, fitur-fitur
    PopBox Loker, serta Sistem dan Standar yang berlaku dengan memberitahukan
    secara tertulis 7 hari sebelumnya kepada Mitra.

			
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    5. 
				
				<strong>TANGGUNG JAWAB</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    5.1. Masing-masing Pihak bertanggung jawab untuk mematuhi hukum yang
    berlaku di Indonesia, termasuk namun tidak terbatas pada kepatuhan terhadap
    kewajiban perpajakan atas penerimaan atau penghasilan masing-masing dari
    pelaksanaan Perjanjian ini; serta membebaskan dan mengindemnifikasi Pihak
    lainnya atas setiap tuntutan, gugatan dan/atau klaim dari pihak ketiga
    manapun akibat ketidakpatuhan terhadap hukum tersebut.
</p>
			<p align="justify">
    5.2. Mitra memahami bahwa setiap barang konsumen yang ditempatkan dalam
    Popbox Loker adalah tanggung jawab konsumen yang bersangkutan; oleh karena
    itu, Mitra tidak akan meminta pertanggungjawaban dalam bentuk apapun kepada
    PopBox atas segala risiko yang timbul akibat penempatan barang tersebut.
</p>
			<p align="justify">
    5.3. Masing-masing Pihak dibebaskan dari tanggung jawab atas kegagalan
    dalam melaksanakan kewajibannya berdasarkan Perjanjian akibat terjadinya
    Keadaan Kahar. Keadaan Kahar adalah suatu keadaan di luar kendali
    masing-masing Pihak yaitu bencana alam, banjir, kebakaran, gempa bumi,
    perang, huru hara, kerusuhan, serangan masa, wabah penyakit yang secara
    langsung membatasi kemampuan Pihak tersebut untuk melaksanakan kewajibannya
    berdasarkan perjanjian ini.
</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    6. 
				
				<strong>REKENING</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    Setiap pembayaran oleh satu Pihak kepada Pihak lainnya berdasarkan
    Perjanjian ini akan dilakukan melalui transfer ke rekening bank berikut ini
    dengan mencantumkan rincian peruntukan pembayaran pada slip transfer.
</p>
			<p align="justify">
				<strong>Pembayaran kepada PopBox ditujukan ke:</strong>
				<strong></strong>
			</p>
			<p align="justify">
    Rekening No. 808-800-1946 di Bank BNI cabang Senayan atas nama PT Popbox
    Asia Services; atau
</p>
			<p align="justify">
    Rekening No. 526-035-8822 di Bank BCA cabang KS Tubun atas nama PT Popbox
    Asia Services.
</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    7. 
				
				<strong>PENGALIHAN</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    Masing-masing Pihak tidak akan mengalihkan setiap hak dan kewajibannya
    dalam Perjanjian ini kepada pihak ketiga tanpa persetujuan tertulis
    terlebih dahulu dari Pihak lainnya.
</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    8. 
				
				<strong>PENGAKHIRAN</strong>
				<strong></strong>
			</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    8.1. PopBox dapat mengakhiri Perjanjian ini lebih awal apabila Mitra
    melakukan kelalaian atau pelanggaran atas ketentuan dalam Perjanjian ini
    yang tidak dapat diperbaiki oleh Mitra dalam jangka waktu yang ditentukan
    berdasarkan surat teguran dari PopBox.
</p>
			<p align="justify">
    8.2. Terlepas dari ketentuan Klausul 8.1., PopBox dapat mengakhiri
    Perjanjian ini dengan memberikan pemberitahuan secara tertulis […] hari
    sebelumnya kepada Mitra apabila Mitra:
</p>
			<p align="justify">
    a. tidak dapat bekerja sama dengan baik berdasarkan pendapat PopBox; atau
</p>
			<p align="justify">
    b. melanggar Sistem dan Standar PopBox; atau
</p>
			<p align="justify">
    c. melanggar eksklusifitas sebagaimana dimaksud dalam Klausul 3.1.d; atau
</p>
			<p align="justify">
    d. ditetapkan pailit atau berada di bawah pengawasan kurator berdasarkan
    putusan pengadilan.
</p>
			<p align="justify">
    8.3. Dalam hal Perjanjian berakhir lebih awal berdasarkan Klausul 8 ini:
</p>
			<p align="justify">
    a. Pihak Pertama berhak menentukan besaran kompensasi yang akan diberikan
    kepada Mitra;
</p>
			<p align="justify">
    b. Mitra wajib mengembalikan segala asset/barang milik Popbox ataupun
    pelanggan kepada PopBox dalam keadaan baik dan benar;
</p>
			<p align="justify">
    c. Mitra tidak akan menuntut pengembalian Biaya Sewa dan biaya-biaya lain
    yang sudah diberikan oleh Mitra kepada PopBox.
</p>
			<p align="justify">
    8.4. Pada saat berakhirnya Masa Perjanjian atau berakhirnya Perjanjian
    berdasarkan Klausul 8 ini:
</p>
			<p align="justify">
    a. Mitra wajib membayar secara penuh seluruh kewajiban Mitra yang telah
    timbul kepada PopBox berdasarkan Perjanjian dalam waktu paling lambat 30
    (tiga puluh) hari setelah tanggal berakhirnya Perjanjian;
</p>
			<p align="justify">
    b. Mitra mengembalikan seluruh barang dan/atau peralatan yang telah
    dipinjamkan oleh PopBox dalam rangka pelaksanaan Perjanjian ini, termasuk
    namun tidak terbatas pada buku manual, video, kaset, formulir, dan
    barang-barang cetakan lainnya dalam waktu paling lambat 14 (empat belas)
    hari setelah tanggal berakhirnya Perjanjian.
</p>
			<p align="justify">
    8.5. Para Pihak mengesampingkan ketentuan Pasal 1266 Kitab Undang-Undang
    Hukum Perdata sehubungan dengan persyaratan permohonan pembatalan kepada
    pengadilan.
</p>
			<p align="justify">
				<strong></strong>
			</p>
			<p align="justify">
    9. 
				
				<strong>HUKUM DAN PENYELESAIAN PERSELISIHAN</strong>
				<strong></strong>
			</p>
			<p align="justify">
    9.1. Perjanjian ini dibuat dan dilaksanakan berdasarkan hukum Indonesia.
</p>
			<p align="justify">
    9.2. Para Pihak akan berupaya terbaik untuk menyelesaikan setiap
    perselisihan yang timbul dalam pelaksanaan Perjanjian ini secara musyawarah
    dalam waktu 7 (tujuh) hari sejak adanya pemberitahuan tertulis dari salah
    satu Pihak kepada Pihak lainnya terkait perselisihan tersebut.
</p>
			<p align="justify">
    9.3. Apabila Para Pihak tidak dapat menyelesaikan perselisihan secara
    musyawarah dalam jangka waktu sebagaimana dimaksud pada Klausul 9.2, Para
    Pihak akan menyelesaikan perselisihan tersebut melalui Pengadilan Negeri
    Jakarta Barat.
</p>
			<p>
    10. 
				
				<strong>HUBUNGAN ANTARA PARA PIHAK</strong>
				<strong></strong>
			</p>
			<p align="justify">
    10.1. Perjanjian ini tidak ditujukan untuk atau Para Pihak tidak memiliki
    tujuan untuk membentuk persekutuan perdata, perkumpulan, konsorsium atau
    bentuk kerjasama apapun yang membuat Para Pihak bertanggungjawab secara
    tanggung renteng. Perjanjian ini tidak dapat dianggap sebagai bentuk
    penyerahan kewenangan dari salah satu Pihak ke Pihak lainnya untuk
    bertindak sebagai agen, kuasa, atau pegawainya. Perjanjian ini tidak dapat
    ditafsirkan dengan cara apa pun sebagai perjanjian kerja yang menimbulkan
    hubungan buruh majikan.
</p>
			<p align="justify">
    10.2. Judul yang terdapat dalam Perjanjian ini hanya bertujuan sebagai
    referensi dan identifikasi semata dan tidak dapat menjadi dasar bagi Para
Pihak untuk menafsirkan bagian mana pun dari Perjanjian ini.    
				
				<strong></strong>
				<strong></strong>
			</p>
		</body>
	</html>